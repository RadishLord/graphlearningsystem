#ifndef DEMONSTRATIONSTEP_H
#define DEMONSTRATIONSTEP_H

#include <QList>
#include <graph.h>

class DemonstrationStep
{
public:
    int curNode;
    QList<int> GreyNode;
    QList<int> GreenNode;
    QList<int> BlueNode;
    QList<int> queue;
    QString comment;
public:
    DemonstrationStep();
    DemonstrationStep(int, QList<int>&, QList<int>&, QList<int>&, QList<int>&, QString);
    DemonstrationStep *init(int, QList<int> *, QList<int> *, QList<int> *, QList<int> *, QString);
};

#endif // DEMONSTRATIONSTEP_H
