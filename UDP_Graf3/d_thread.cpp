#include "d_thread.h"
#include <QDebug>

d_Thread::d_Thread(QString threadName) :
    name(threadName)
{

}


void d_Thread::run()
{
    this->m_mainWindow.bfs();
}

void d_Thread::setForm(MainWindow *mainWindow)
{
    if (m_mainWindow == &mainWindow)
        return;

    m_mainWindow = &mainWindow;
    emit nameChanged(mainWindow);
}
