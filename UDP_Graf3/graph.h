#ifndef GRAPH_H
#define GRAPH_H
#include "qvector.h"
#include "qlist.h"
#include "QColor"

struct Vertex
{
    int x;
    int y;
    int number;
    //char symbol;
    QColor color;
    //int parent;
    //int level;
public:
    Vertex(int n);
    Vertex();
};

class Graph
{
    QVector <QVector<Vertex>> Adj;   //--список смежностей
    int **Amr;                     //--матрица смежности

    int VCount;                    //--кол-во вершин
    int ECount;                    //--кол-во рёбер

public:
    Graph();
    ~Graph();
    void initFromFile(QString);
    int getVCount();
    int getECount();
    int AdjAt(int i, int j);
    int AmrAt(int i, int j);
    int getAdjCount(int i);
    void fromADJtoAMR();
    Vertex getVertex(int,int);
    Vertex getVertexWithNumber(int);
    void setVertexColor(int, int, QColor);
    void setVertexPos(int, int, int, int);
    void clear();
    void arrangementGraph();//--укладывает вершины на сцене
    Graph bfs();
};

#endif // GRAPH_H
