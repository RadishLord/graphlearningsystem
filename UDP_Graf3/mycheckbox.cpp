#include "mycheckbox.h"

MyCheckBox::MyCheckBox()
{

}

MyCheckBox::MyCheckBox(Distractor dstr)
{
    this->setText(dstr.text);
    this->right  = dstr.right;
    this->number = dstr.number;

    QFont font = this->font();         //--шрифт
    font.setPointSize(16);             //--размер текста 16, как и чекбоксов в классик
    this->setFont(font);               //--установка шрифта
}
