#ifndef GRAPHDESIGNER_H
#define GRAPHDESIGNER_H
#include <QGraphicsScene>

#include "QStandardItemModel"
#include "QStandardItem"
#include <QtGui>
#include <QtWidgets>
#include <QDebug>
#include <QGraphicsItem>
#include <QTextEdit>
#include <QGraphicsScene>
#include "graph.h"

class  GraphDesigner
{
public:
    QGraphicsScene scene;

    GraphDesigner();
    void  drawGraph(Graph *);
    void  repaint(Vertex );
    void arrowDraw(Vertex v1, Vertex v2);
    void allRepaint(Graph *graph);
    void  vertexDraw(Vertex, QColor);
private:
    void  vertexDraw(Vertex);
    void  edgeDraw(Vertex, Vertex);
    void  arrangementGraph(Graph *);
};



#endif // GRAPHDESIGNER_H
