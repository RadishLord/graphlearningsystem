#ifndef TITLEPAGE_H
#define TITLEPAGE_H

#include <QMainWindow>

namespace Ui {
class titlePage;
}

class titlePage : public QMainWindow
{
    Q_OBJECT

public:
    explicit titlePage(QMainWindow *parent = 0);
    ~titlePage();
private slots:
   void on_interButton_clicked();

private:
    Ui::titlePage *ui;
};

#endif // TITLEPAGE_H
