#ifndef MYLABEL_H
#define MYLABEL_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "question.h"

class MyLabel : public QLabel
{
    Q_OBJECT
public:
    QWidget *obj;   //--ссылка на родитель
    bool isMoved;   //--отметка для передвигаемых лейблов
    bool isMoveble; //--true если подвижный объект
    int cell;       //--от 1 до 4, номер текущей ячейки
    int number;     //--от 1 до 4, номер дистрактора для проверки
    bool right;     //--верность ответа
public:
    MyLabel();

    void init(Distractor dstr);
    void initMoveble(Distractor dstr);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

signals:
    void signalDistReplace(int, int);

};

#endif // MYLABEL_H
