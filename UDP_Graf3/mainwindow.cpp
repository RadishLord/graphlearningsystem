#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mylabel.h"
#include "mycheckbox.h"
#include <QMessageBox>
#include <QFile>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    theoryDisplay();    //--вывод теории
    setUnVisbleUI();    //--скрывает элементы интерфейса
    TestReset();        //--ребут теста

    graphInit();        //--загрузка графа


   ui->graphicsView->setScene(&this->graphDesigner.scene);

}

MainWindow::~MainWindow()
{

    delete ui;

}

//--информация об авторе программы
void MainWindow::on_actInfo_triggered()
{
    QMessageBox::about(this, tr("Об авторе"),
    tr("Программу выполнил студент группы ДИПР-21 Кучикаев Данил.\nИнститут информационных технологий и коммуникаций.\nАстраханский государственный технический университет."));
}
//--Закрытия главного окна
void MainWindow::on_actExit_triggered()
{
     close();
}
//--Вывод теории на экран
void MainWindow::theoryDisplay()
{
    QFile f("./resources/theory/theory.html");
    QString s;
    if(f.open(QIODevice::ReadOnly))
        s = QString::fromUtf8(f.readAll());
    else
        s = "Теория не найдена :(";
    ui->theoryField->setHtml(s);
    f.close();
}



