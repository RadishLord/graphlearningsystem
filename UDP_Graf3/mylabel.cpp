#include "mylabel.h"
#include <QMessageBox>
#include <QMouseEvent>
#include "mainwindow.h"
#include "mainwindow.cpp"
#include <QLabel>

MyLabel::MyLabel()
{

}
void MyLabel::init(Distractor dstr)
{
    this->setText(dstr.text);
    this->right  = dstr.right;
    this->number = dstr.number;
    this->isMoved = false;
    this->isMoveble = false;
    QFont font = this->font();         //--шрифт
    font.setPointSize(16);             //--размер текста 16, как и чекбоксов в классик
    this->setFont(font);               //--установка шрифта
}
void MyLabel::initMoveble(Distractor dstr)
{
    this->setText(dstr.text);
    this->right  = dstr.right;
    this->number = dstr.number;
    this->isMoved = false;
    this->isMoveble = false;
    this->cell = dstr.cell;
    this->isMoveble = true;
    QFont font = this->font();         //--шрифт
    font.setPointSize(16);             //--размер текста 16, как и чекбоксов в классик
    this->setFont(font);               //--установка шрифта
}
//////////////////////
QPoint lastPoint;
bool b_move;


void MyLabel::mousePressEvent(QMouseEvent *event)
{
    //QMessageBox::information(0,"1","1");
    QRect p = this->geometry();
    if(event->button() == Qt::LeftButton && this->isMoveble)
    {
        lastPoint = event->pos();
        b_move = true;
    }
}

void MyLabel::mouseMoveEvent(QMouseEvent *event)
{

    if((event->buttons() & Qt::LeftButton) && b_move && this->isMoveble)
      {
        QPoint point;
        point.setX(event->pos().x() - lastPoint.x());
        point.setY(event->pos().y() - lastPoint.y());
        this->move(mapToParent(point));
      }
}

void MyLabel::mouseReleaseEvent(QMouseEvent *event)
{

    if (event->button() == Qt::LeftButton && b_move && this->isMoveble)
    {
        //--запоминаем старое расположение
        int oldCell = this->cell;
        //--проверка и подгон финального расположения
         QPoint p;
         p.setX(500);

        if (this->y() < 230)
        {
           p.setY(200);
           this->cell = 1;
        }
        else if (this->y() > 230 && this->y() < 280)
        {
            p.setY(250);
            this->cell = 2;
        }
        else if(this->y() > 270 && this->y() < 330)
        {
            p.setY(300);
            this->cell = 3;
        }
        else if (this->y() > 330)
        {
            p.setY(350);
            this->cell = 4;
        }
        this->setGeometry(p.x(), p.y(), 400, 30);

        //--перегруппировка ответов

        this->isMoved = true;
        QObject::connect(this, SIGNAL(signalDistReplace(int, int)), this->obj, SLOT(slotDistReplace(int, int)));
        emit signalDistReplace(oldCell, cell);
        b_move = false;
    }


}
