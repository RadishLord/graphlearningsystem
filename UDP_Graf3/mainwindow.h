#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QMessageBox>
#include <QDebug>
#include <QVector>
#include <QDir>
#include <QRadioButton>


#include "mylabel.h"
#include "question.h"
#include "graph.h"
#include "graphdesigner.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    GraphDesigner graphDesigner;

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void theoryDisplay();
    int popQueue(QList<int> *queue);
    void addToQueue(QList<int> *queue, QList<int> *Green, int i);
    void queueDisplay(QList<int> queue, int curr, QString str);
    void stepDisplay(int i);
    void stepCounterDisplay(int i, int n);
    int setSize(QString str);
    void dfs();
    void dfsVisit(Graph *graph, int v);
    bool dfcCheck(int v);
    void stackDisplay(QList<int>, QString str);
private slots:
    void on_actInfo_triggered();    //--Об авторе
    void on_actExit_triggered();    //--выход
    void on_testButton_clicked();   //--кнопка "начать тест"
    void on_nextButton_clicked();   //--кнопка "сдежующий вопрос"
    void questDisplay( Question);
    void classicQuestDisplay(Question);
    void conformQuestDisplay(Question);
    void setUnVisbleUI();      //--скрывает все дистракторы
    void on_endTestButton_clicked();
    void TestReset();
    void answCheck();
    void classicAnswCheck();
    void conformAnswCheck();
    void graphInit();
    void graphInTable();
    void on_loadGraph_triggered();
    void graphDisplay();
    void graphInAdj();
    void graphDraw();
    void bfs();


    void on_bfsRadio_clicked();

    void on_pbStepForward_clicked();

    void on_pbStepBack_clicked();

    void on_dfsRadio_clicked();

public slots:
    void slotDistReplace(int oldCell, int cell);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
