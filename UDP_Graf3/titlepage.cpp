#include "titlepage.h"
#include "ui_titlepage.h"
#include "mainwindow.h"
#include <QMessageBox>

titlePage::titlePage(QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::titlePage)
{
    ui->setupUi(this);
}

titlePage::~titlePage()
{
    delete ui;
}
//  --  кнопка "начать тест"
void titlePage::on_interButton_clicked()
{
    QMainWindow* window = new MainWindow(this);
    window->show();
    //this->hide();
}
