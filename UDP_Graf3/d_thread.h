#ifndef D_THREAD_H
#define D_THREAD_H

#include <QThread>
#include "mainwindow.h"
class d_Thread : public QThread
{
    Q_PROPERTY(MainWindow mainWindow READ name WRITE setName NOTIFY nameChanged)
public:

    explicit d_Thread(QString threadName);


    void run();
    MainWindow form() const
    {
        return m_mainWindow;
    }

public slots:
    void setForm(MainWindow *mainWindow);

signals:
    void FormChanged(MainWindow mainWindow);

private:
    QString name1;   //--имя потока
    MainWindow m_mainWindow;
};

#endif // D_THREAD_H
