#include "demonstrationstep.h"

DemonstrationStep::DemonstrationStep()
{

}

DemonstrationStep::DemonstrationStep(int curNode, QList<int> &GreyNode, QList<int> &GreenNode, QList<int> &BlueNode, QList<int> &queue, QString comment)
{
    this->curNode = curNode;
    this->GreyNode = GreyNode;
    this->GreenNode = GreenNode;
    this->BlueNode = BlueNode;
    this->queue = queue;
    this->comment = comment;
}

DemonstrationStep* DemonstrationStep::init(int curNode, QList<int> *GreyNode, QList<int> *GreenNode, QList<int> *BlueNode, QList<int> *queue, QString comment)
{

    this->curNode = curNode;
    this->GreyNode = *GreyNode;
    this->GreenNode = *GreenNode;
    this->BlueNode = *BlueNode;
    this->queue = *queue;
    this->comment = comment;
    return this;
}
