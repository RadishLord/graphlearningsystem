#-------------------------------------------------
#
# Project created by QtCreator 2016-12-18T22:47:26
#
#-------------------------------------------------

#QMAKE_CXXFLAGS += -pthread -std=c++11
#QMAKE_LFLAGS += -Wl,--no-as-needed


#LIBS += -lpthread -lrt

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UDP_Graf3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    titlepage.cpp \
    mylabel.cpp \
    mycheckbox.cpp \
    test.cpp \
    demonstration.cpp \
    graph.cpp \
    graphdesigner.cpp \
    demonstrationstep.cpp

HEADERS  += mainwindow.h \
    titlepage.h \
    question.h \
    mylabel.h \
    mycheckbox.h \
    graph.h \
    graphdesigner.h \
    demonstrationstep.h

FORMS    += mainwindow.ui \
    titlepage.ui

DISTFILES +=
