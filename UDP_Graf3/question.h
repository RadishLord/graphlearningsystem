#ifndef QUESTION
#define QUESTION
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <string>
#include <QString>
#include <QMessageBox>
#include <iostream>
#include <fstream>
#include <sstream>
#include <QList>

using namespace std;
//--дистрактор для вопроса типа conform
struct Distractor
{
    QString text;
    bool right = false;
    int number;
    int cell;

};

class Question
{
public:
    enum questType
    {
        classic,                //--классический тип вопроса
        conform                 //--вопрос на соответствие
    };

    questType type;                 //--тип вопроса
    int cost;                       //--стоимость (сложность) вопроса
    QString text;                   //--текст вопроса
    QList <int> answer;              //--номера правильных ответов
    QList <Distractor> distractor;  //--варианты ответа
    QList <Distractor> distractor2; //--варианты ответа для типа конформ

    int distCount;                  //--количество дистракторов
    int answerCount;

public:
    //--интерпретация классич вопроса
    void classicInit(QTextStream &stream)
    {
        QString str;                                                                 //-- Объяв:строка для временого хранения типа вопроса и разделителей
        this->type = classic;
        str = stream.readLine();                                                     //-- Считываем номер вопроса
        this->cost = str.toInt();                                                    //-- записываем номер в поле
        str = stream.readLine();                                                     //-- Считываем текст вопроса
        this->text = str;                                                            //-- записываем текст вопроса в поле
        //--считывает и записывает в поле номера правильных ответов
        str = stream.readLine();
        QString newStr;
        QTextStream ss(&str);
        int i = 0;
        while (true)
        {
            ss >> newStr;
            if (newStr == "#") break;
            this->answer.append(newStr.toInt());
            ++i;
        }
        this->answerCount = i;
        //--считывает и записывает в поле варианты ответов
        i = 0;
        while (true)
        {
            Distractor *tempDstr = new Distractor();      //--экземпляр дистрактора
            str = stream.readLine();                      //считываем очередной дистрактор
            if (str == "#end") break;
            //this->distractor[i]=str;//-----------------------------
            tempDstr->text = str;
            this->distractor.append(*tempDstr);
            ++i;
        }
        this->distCount = i;            //записываем количество дистракторов в поле
        //--иниц. правильных ответов
        for (int i = 0; i < answer.count(); ++i)
        {
            int j = answer[i];
            this->distractor[j-1].right = true;
        }

    }
    //--интерпретация вопроса на соответствие
    void conformInit(QTextStream &stream)
    {
        QString str;                                //-- Объяв:строка для временого хранения типа вопроса и разделителей
        this->type = conform;
        str = stream.readLine();                    //-- Считываем номер вопроса
        this->cost = str.toInt();                 //-- записываем номер в поле
        str = stream.readLine();                    //-- Считываем текст вопроса
        this->text = str;                           //-- записываем текст вопроса в поле

        //--считывает и записывает в поле дистракторы 1
        int i = 0;
        while (true)//--выходит при встрече "#" в потоке
        {
            Distractor *tempDstr = new Distractor();      //--экземпляр дистрактора
            str = stream.readLine();                      //--считываем очередной дистрактор
            if (str == "#") break;                        //--тег конца перечисления
            tempDstr->text = str;                         //--текст дистра
            tempDstr->number = i+1;                         //--независимый номер
            this->distractor.append(*tempDstr);           //--добавляем в список
            ++i;
        }
        this->distCount = i;            //записываем количество дистракторов в поле
        //--считывает и записывает в поле дистракторы 2
        i = 0;
        while (true)//--выходит из цикла при встрече "#end"
        {
            Distractor *tempDstr = new Distractor();      //--экземпляр дистрактора
            str = stream.readLine();                      //--считываем очередной дистрактор
            if (str == "#end" ) break;                    //--тег конца перечисления
            tempDstr->text = str;                         //--текст дистра
            tempDstr->number = i+1;                         //--независимый номер
            tempDstr->cell = i+1;                           //--зависимый от ячейки номер
            this->distractor2.append(*tempDstr);          //--добавляем в список
            ++i;
        }
    }
    //--перемшка дистракторов(qstring)
    void distractorMix()
    {
        switch (type)
        {
        case classic:
            {
                for (int i = 0; i < distCount; ++i)
                {//--перемешка дистров
                    distractor.swap(i, rand()%distCount); 
                }

            } break;
        case conform:
            {

                for (int i = 0; i < distCount; ++i)
                {
                    int r = rand()%distCount;
                    distractor.swap(i, r);
                    distractor2.swap(i, r);

                    int tempCell = distractor2[i].cell;
                    distractor2[i].cell = distractor2[r].cell;
                    distractor2[r].cell = tempCell;
                    ////////
                 }
                //--переприсвоение number
                for (int i = 0; i < distCount; ++i)
                {
                    distractor2[i].number = i+1;
                }
                for (int i = 0; i < distCount; ++i)
                {
                    int r = rand()%distCount;
                    distractor2.swap(i, r);

                    int tempCell = distractor2[i].cell;
                    distractor2[i].cell = distractor2[r].cell;
                    distractor2[r].cell = tempCell;
                 }

            } break;
        default:
            break;
        }

    }
};








//////////////////////////////////////////////////////////////////////////////////////////////////
class QuestList
{

public:
    QList <Question> quest;


public:
    //--очистка листа
    void clear()
    {
        this->quest.clear();
    }
    //--возвращает размер листа
    int count()
    {
        return this->quest.count();
    }
    //--перемешивает содержимое
    void mix()
    {
        for (int i = 0; i < quest.count(); ++i)        //--перемешка вопросов
            quest.swap(i, rand()%quest.count());
    }
    //--загружает список вопросов в лист
    void load(const QString fileName)
    {
        QFile mFile(fileName);                                                      //-- Открытие файла
        if(!mFile.open(QFile::ReadOnly | QFile::Text))                              //-- Проверка на открытие файла. (только для чтения | это текст)
        {
            QMessageBox::information(0,"Error", "База вопросов не найдена");
           // return;
        }
        QTextStream stream(&mFile);                                                  //-- Работает как fstream
        QString type;

        while (!stream.atEnd())
        {
            Question tempQuest;
            type = stream.readLine();                                                    //--считываем тип вопроса

            if (type == "classic")
            {
                tempQuest.classicInit(stream);
            }
            else if (type == "conform")
            {
               tempQuest.conformInit(stream);
            }

            quest << tempQuest;
        }
        mFile.close();
    }
};

#endif // QUESTION

