#ifndef MYCHECKBOX_H
#define MYCHECKBOX_H

#include <QWidget>
#include <QCheckBox>
#include <QMouseEvent>
#include "mainwindow.h"
#include "ui_mainwindow.h"


class MyCheckBox : public QCheckBox
{
    Q_OBJECT
public:
    QWidget *obj;   //--ссылка на родитель
    bool isMoved;   //--отметка для передвигаемых чекбоксов
    int cell;       //--от 1 до 4, номер текущей ячейки
    int number;     //--от 1 до 4, номер дистрактора для проверки
    bool right;     //--верность ответа

public:
    MyCheckBox();
    MyCheckBox(Distractor dstr);
};

#endif // MYCHECKBOX_H

