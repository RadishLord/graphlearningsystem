#include "graphdesigner.h"
//--конструктор
GraphDesigner::GraphDesigner()
{

}
//--перересовывает все вершины
void GraphDesigner::allRepaint(Graph *graph)
{
    for (int i = 0; i < graph->getVCount(); ++i)
    {
        for(int j = 0; j < graph->getAdjCount(i); ++j)
        {
            repaint(graph->getVertex(i,j));
        }
    }

}
//--перерисовывает вершину
void GraphDesigner::repaint(Vertex v)
{
    vertexDraw(v);
}
//--Рисует граф
void  GraphDesigner::drawGraph(Graph *graph)
{
    //--рисует ребра и вершины графа
    for (int i = 0; i < graph->getVCount(); ++i)
    {
        for (int j = 1; j < graph->getAdjCount(i); ++j)
        {
            int num = graph->getVertex(i,j).number;
            edgeDraw( graph->getVertex(i,0), graph->getVertex(num-1, 0));
        }
    }
    for (int i = 0; i < graph->getVCount(); ++i)
    {
        vertexDraw(graph->getVertex(i,0));
    }
}

//--рисует вершину
void  GraphDesigner::vertexDraw(Vertex v)
{
    scene.addEllipse(QRectF(v.x, v.y, 50.0, 50.0), QPen(Qt::black, 3), QBrush(v.color));
    //QGraphicsTextItem *textItem = new QGraphicsTextItem(QString("%1").arg(v.number));
    QGraphicsTextItem *textItem = new QGraphicsTextItem(QString::number(v.number));
    textItem->setFont(QFont("Times new roman",25,25));
    if (v.number < 10)  //--задает смещение цифры
        textItem->setPos(v.x + 11, v.y +5);
    else
        textItem->setPos(v.x + 6, v.y +5);

    scene.addItem(textItem);


}
//--рисует вершину
void  GraphDesigner::vertexDraw(Vertex v, QColor color)
{

    scene.addEllipse(QRectF(v.x, v.y, 50.0, 50.0), QPen(Qt::black, 3), QBrush(color));
    //QGraphicsTextItem *textItem = new QGraphicsTextItem(QString("%1").arg(v.number));
    QGraphicsTextItem *textItem = new QGraphicsTextItem(QString::number(v.number));
    textItem->setFont(QFont("Times new roman",25,25));
    if (v.number < 10)  //--задает смещение цифры
        textItem->setPos(v.x + 11, v.y +5);
    else
        textItem->setPos(v.x + 6, v.y +5);

    scene.addItem(textItem);


}
//--рисует линию между вершинами
void  GraphDesigner::edgeDraw(Vertex v1, Vertex v2)
{
    scene.addLine(v1.x+25,v1.y+25, v2.x+25, v2.y+25, QPen(Qt::black, 2));
}
//--рисует стрелку между вершинами del
void  GraphDesigner::arrowDraw(Vertex v1, Vertex v2)
{
    //scene.addLine(v1.x+25,v1.y+25, v2.x+25, v2.y+25, QPen(Qt::black, 2));
    QGraphicsEllipseItem  *e = new QGraphicsEllipseItem(v1.x+25,v1.y+25, 5,6, nullptr);
    e->setSpanAngle(5);
    //scene.addEllipse(e);
    scene.addItem(e);
}
