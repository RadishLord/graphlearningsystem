#include "titlepage.h"

#include "mainwindow.h"
#include <QApplication>
#include <QTime>
#include <QTranslator>
#include <QFile>
#include "QTime"


int main(int argc, char *argv[])
{    
    QApplication a(argc, argv);

    //Случайные числа
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));

    // Русский язык
    QTranslator translator;
    translator.load(":/translations/qtbase_ru.qm");
    a.installTranslator(&translator);

    

//    MainWindow mainWind;
//    mainWind.show();

    //вызов титульного листа
    titlePage _titlepage;
    _titlepage.show();

    return a.exec();
}
