#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mylabel.h"
#include "mycheckbox.h"
#include <QMessageBox>
#include <QFile>
#include <QMouseEvent>

//  --  ТЕСТ    --  //


int tempQuestIndex;                 //--индекс текщего вопроса
int CorrectAnswersProcent;          //--процент правильных ответов
float userScore;                    //--кол-во баллов за ответы юзера
float totalScore;                   //--кол-во баллов за все правильные ответы текущей 10ки
QuestList questList;                //--содержит лист вопросов
QList<MyLabel*> MylabelList;        //--лист виджетов
QList<MyCheckBox*> MyCheckBoxList;

//--Кнопка начать тест
void MainWindow::on_testButton_clicked()
{
    questList.load("./resources/questions.txt");    //--загружаем список вопросов
    questList.mix();                                //--перемешка
    ui->testButton->hide();                         //--скрываем кнопку начать тест
    ui->nextButton->show();                         //--показываем кнопку след. вопрос
    ui->endTestButton->show();                      //--показываем кнопку конец теста

    on_nextButton_clicked();                        //--показываем первый (0) вопрос
}
//--Кнопка след. вопрос
void MainWindow::on_nextButton_clicked()
{
    const int LAST_QUEST_INDEX = 9;
    answCheck();                                        //--проверка ответа на текущий вопрос
    ++tempQuestIndex;                                   //--переход к следующему
    setUnVisbleUI();                                    //--установка видимости необходимых элементов
    questDisplay(questList.quest[tempQuestIndex]);      //--вывод вопроса на экран
    totalScore += questList.quest[tempQuestIndex].cost; //--плюсует стоимость текущего вопроса..
                                                        //--...к общей стоимости теста

    if (tempQuestIndex == LAST_QUEST_INDEX || tempQuestIndex == questList.count() - 1)      //-- если последний вопрос
    {
        ui->nextButton->hide();                         //--скрывает кнопку "слде. вопрос"
        ui->endTestButton->setText("Узнать результат"); //--переим. кнопку "закончить тест"
    }
}
//--Кнопка закончить тест
void MainWindow::on_endTestButton_clicked()
{
    const int LAST_QUEST = 9;
    //--проверка последнего ответа
    answCheck();

    //--если тест прерван
    if (tempQuestIndex < LAST_QUEST && tempQuestIndex < questList.count() - 1)
    {
      ui->questBox->setText(QString("Тест был прерван"));
    }
    else
    {
        float procent = -1;
        procent = (100 * userScore)/totalScore;
        //ui->questBox->setText(QString("Результат прохождения теста:%1 баллов из %1").arg(userScore, totalScore));
        ui->questBox->setText(QString("Уровень усвоения материала: %1%").arg(procent));
    }
    TestReset();
}
//--вывод вопроса на экран
void MainWindow::questDisplay(Question quest)
{
    //ui->questBox->setText(quest.text);  //--текст вопроса
    switch (quest.type)
    {
    case quest.classic:
        classicQuestDisplay(quest);
        break;
    case quest.conform:
        conformQuestDisplay(quest);
        break;
    default:
        break;
    }
}
//--выводит на экран вопрос в соотвествии с типом
void MainWindow::classicQuestDisplay(Question quest)
{
    QPoint p1(30,200);
    QPoint p2(500,200);
    QPoint p3(30,250);
    QPoint p4(500,250);
    QPoint p5(30,300);
    QPoint p6(500,300);
    QPoint p7(30,350);
    QPoint p8(500,350);
    QPoint p[8] = {p1, p2, p3, p4, p5, p6, p7, p8};

    ui->questBox->setText(quest.text);  //--текст вопроса
    quest.distractorMix();              //--перемешка дистров
    for (int i = 0; i < quest.distractor.count(); ++i)
    {
        MyCheckBox *tempCB = new MyCheckBox(quest.distractor[i]);


        QFont font = tempCB->font();             //создание объекта класса QFont копированием свойсв QFont у QLabel
        int fontSize = setSize(quest.distractor[i].text);
        font.setPointSize(fontSize);                  //установка высоты шрифта, в данном случае 20
        tempCB->setFont(font);                   //установка изменненного шрифта (QFont) объекту класса QLabel


        tempCB->setGeometry(p[i].x(), p[i].y(), 450, 30);
        tempCB->setParent(ui->tab_3);              //--установка родителя для виджета
        tempCB->show();
        MyCheckBoxList.append(tempCB);
    }
}
void MainWindow::conformQuestDisplay(Question quest)
{
    int fontSize = 19;

    QPoint p1(15,200);
    QPoint p2(15,250);
    QPoint p3(15,300);
    QPoint p4(15,350);
    QPoint p5(500,200);
    QPoint p6(500,250);
    QPoint p7(500,300);
    QPoint p8(500,350);
    QPoint pm1[4] = {p1, p2, p3, p4};
    QPoint pm2[4] = {p5, p6, p7, p8};
    //--инициализация ui элементов
    //--создание первого списка вариантов

    ui->questBox->setText(quest.text);  //--текст вопроса
    quest.distractorMix();              //--перемешка дистров
    for (int i = 0; i < quest.distractor.count(); ++i)
    {
        MyLabel *tempLB = new MyLabel();
        tempLB->init(quest.distractor[i]);
        tempLB->setGeometry(pm1[i].x(), pm1[i].y(), 450, 30);
        tempLB->setParent(ui->tab_3);              //--установка родителя для виджета

        QFont font = tempLB->font();             //создание объекта класса QFont копированием свойсв QFont у QLabel
        fontSize = setSize(quest.distractor[i].text);
        font.setPointSize(fontSize);                  //установка высоты шрифта, в данном случае 20
        tempLB->setFont(font);                   //установка изменненного шрифта (QFont) объекту класса QLabel

        //QMessageBox::information(this, "fd", QString("%1").arg(tempLB->text().count()));
        tempLB->show();
        MylabelList.append(tempLB);
    }
    //--Создание подвижного (второго) списка
    for (int i = 0; i < quest.distractor2.count(); ++i)
    {
        MyLabel *tempLB = new MyLabel();
        tempLB->initMoveble(quest.distractor2[i]);
        tempLB->setGeometry(pm2[i].x(), pm2[i].y(), 450, 30);
        tempLB->setParent(ui->tab_3);              //--установка родителя для виджета

        QFont font = tempLB->font();             //создание объекта класса QFont копированием свойсв QFont у QLabel
        fontSize = setSize(quest.distractor2[i].text);
        font.setPointSize(fontSize);                  //установка высоты шрифта, в данном случае 20
        tempLB->setFont(font);                   //установка изменненного шрифта (QFont) объекту класса QLabel

        tempLB->show();
        tempLB->obj = this;
        MylabelList.append(tempLB);
    }
}
//--принимает строку возвращает размер шрифата
int MainWindow::setSize(QString str)
{
    int res = str.length() / 10;
    res = 10 - res;
    res = res * 2 + 2;
    if (res <= 0) res = 5;
    if (res > 20) res = 20;
    return res;
}

//--скрывает чекбоксы и лейблы с дистракторами
void MainWindow::setUnVisbleUI()
{

    for(int j = 0; j < MylabelList.count(); ++j)
    {
        MylabelList[j]->hide();
        //MylabelList.removeAt(j);
    }
    for(int j = 0; j < MyCheckBoxList.count(); ++j)
    {
        MyCheckBoxList[j]->hide();
        //MyCheckBoxList.removeAt(j);
    }
    MylabelList.clear();
    MyCheckBoxList.clear();
}
//--сброс теста
void MainWindow::TestReset()
{
    ui->endTestButton->setText("Закончить тест");
    setUnVisbleUI();
    ui->nextButton->hide();
    ui->endTestButton->hide();
    //ui->questBox->clear();
    ui->testButton->show();
    tempQuestIndex = -1;
    userScore = 0;
    totalScore = 0;
    CorrectAnswersProcent = 0;
    questList.clear();

}
//--отвечает за премещение дистров на освобдившеися ячейки
void MainWindow::slotDistReplace(int oldCell, int newCell)
{
    bool Continue = false;
    //--это непонятная хрень нужна для предотвращения повторного запуска слота
    for (int i = 0; i < MylabelList.count(); ++i)
    {
        if (MylabelList[i]->isMoved == true)
            Continue = true;
    }
    if(!Continue) return;
    //TODO: почему эта фигня срабатывает больше положеного?

    QPoint p0(500,150);
    QPoint p1(500,200);
    QPoint p2(500,250);
    QPoint p3(500,300);
    QPoint p4(500,350);
    QPoint p[5] = {p0, p1, p2, p3, p4};
    //QMessageBox::information(0, "1", QString("oldCell: %1").arg(oldCell));
    //QMessageBox::information(0, "1", QString("newCell: %1").arg(newCell));
    for (int i = 0; i < MylabelList.count(); ++i)
    {
        if (MylabelList[i]->cell == newCell && !MylabelList[i]->isMoved)
        {
            //--свопаем на освободившеися место
             MylabelList[i]->setGeometry(p[oldCell].x(),p[oldCell].y(),MylabelList[i]->width(),MylabelList[i]->height());
             MylabelList[i]->cell = oldCell;
        }
    }
    //--обнуляем состояние дистров
    for (int i = 0; i < MylabelList.count(); ++i)
        MylabelList[i]->isMoved = false;
}
//--проверка ответа
void MainWindow::answCheck()
{
    Question q;
    if ((tempQuestIndex >= 0))
    {
        switch (questList.quest[tempQuestIndex].type)
        {
        case q.classic:
            classicAnswCheck();
            break;
        case q.conform:
            conformAnswCheck();
            break;
        default:
            break;
        }
    }
}
//--Проверка вопроса типа "classic"
void MainWindow::classicAnswCheck()
{
    float cost = questList.quest[tempQuestIndex].cost;
    float costPer = cost/questList.quest[tempQuestIndex].answer.count();
    float tempScore = 0;
    for (int i = 0; i<MyCheckBoxList.count(); ++i)
    {
        bool isRight = MyCheckBoxList[i]->right;
        bool userAnswer = MyCheckBoxList[i]->isChecked();

        if (isRight && userAnswer)
        {
            //--юзер отметил верный чекбокс
            tempScore += costPer;
        }
        else if (!isRight && userAnswer)
        {
            //--юзер отметил неверный чекбокс
            tempScore -= costPer;
        }
        else if (isRight && !userAnswer)
        {
            //--юзер не отметил верный чекбокс
        }
    }
    if (tempScore < 0) tempScore = 0;
    userScore += tempScore;
    //QMessageBox::information(0,"3", QString("score: %1").arg(userScore));
}
//--Проверка вопроса типа "conform"
void MainWindow::conformAnswCheck()
{
    float cost = questList.quest[tempQuestIndex].cost;
    float costPer = cost/questList.quest[tempQuestIndex].distractor2.count();
    float tempScore = 0;
    for (int i = 0; i<MylabelList.count(); ++i)
    {

        if ((MylabelList[i]->isMoveble == true) && (MylabelList[i]->cell == MylabelList[i]->number))
        {
            //--правильное соотношение
            tempScore += costPer;
        }
        else if ((MylabelList[i]->isMoveble == true) && (MylabelList[i]->cell != MylabelList[i]->number))
        {
            tempScore -= costPer;
        }

    }
    if (tempScore < 0) tempScore = 0;
    userScore += tempScore;

}
