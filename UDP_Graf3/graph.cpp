#include "graph.h"
#include "QFile"
#include "QMessageBox"
#include "QTextStream"


Vertex::Vertex()
{
    this->color = Qt::white;
}
Vertex::Vertex(int n)
{
    this->number = n;
    this->color = Qt::white;
}
Graph::Graph()
{

}
Graph::~Graph()
{
    for (int i = 0; i < this->VCount; ++i)
        delete []this->Amr[i];
    delete []this->Amr;

    //this->Adj.clear();
}
//--задаёт позицию вершины графа
void Graph::setVertexPos(int i, int j, int x, int y)
{
    if (i < this->VCount && j < this->ECount)
    {
        this->Adj[i][j].x = x;
        this->Adj[i][j].y = y;
    }
    else
        throw "Искомого элемента не существует";

}
//--задаёт цвет вершины графа
void Graph::setVertexColor(int i, int j, QColor color)
{
    if (i < this->VCount && j < this->ECount)
        this->Adj[i][j].color = QColor(color);
    else
        throw "Искомого элемента не существует";


}
//--возвращает iую jую вершину
Vertex Graph::getVertex(int i,int j)
{
    if (i < this->VCount && j < this->ECount)
        return this->Adj.at(i).at(j);
    else
        throw "Искомого элемента не существует";
}
//--возвращает вершину c номером i
Vertex Graph::getVertexWithNumber(int n)
{
    n += 1;
    for (int i = 0; i < this->getVCount(); ++i)
    {
        for(int j = 0; j < this->getAdjCount(i); ++j)
        {
            int nn = this->Adj.at(i).at(j).number;

            if (nn == n)
                return this->Adj.at(i).at(j);
        }
    }
    throw "Искомого элемента не существует";
}
//--возвращает кол-во вершин
int Graph::getVCount()
{
    return this->VCount;
}
//--возвращает кол-во рёбер
int Graph::getECount()
{
    return this->ECount;
}
//--возвращает j-ую вершину из i-го списка
int Graph::AdjAt(int i, int j)
{
    if (i < this->VCount && j < this->ECount)
        return this->Adj.at(i).at(j).number;
    else
        throw "Искомого элемента не существует";
}
//--возвращает (i,j) элмент матрицы
int Graph::AmrAt(int i, int j)
{
    if (i < this->VCount && j < this->VCount)
        return this->Amr[i][j];
    else
        throw "Искомого элемента не существует";
}
//--возвращает кол-во смежных вершин i-го списка
int Graph::getAdjCount(int i)
{
    return this->Adj.at(i).count();
}
//--иниц.из файла
void Graph::initFromFile(QString fileName)
{
    QFile mFile(fileName);                                                      //-- Открытие файла
    if(!mFile.open(QFile::ReadOnly | QFile::Text))                              //-- Проверка на открытие файла. (только для чтения | это текст)
       throw QString("Файл с графом не обнаружен");

    QTextStream stream(&mFile);                                                  //-- Работает как fstream
    QString str;
    stream >> str;                       //--считываем..
    this->VCount = str.toInt();          //--..количество вершин

    if (this->VCount <= 1)
        throw QString("Граф задан некорректно");


    this->ECount = 0;

    for (int i = 0; i < this->VCount; ++i)
    {
       QVector<Vertex> tempList;
       Vertex tV;
       stream >> str;       
       tV.number = str.toInt(); // считываем номер вершины
       tempList.append(tV);

       stream >> str;       // считываем "->"


       while (true)
       {
           Vertex tVertex;
           stream >> str;   //--считываем смежную вершину
           if (str == "#" || stream.atEnd())
           {
               break;
           }
           tVertex.number = str.toInt();
           tempList.append(tVertex);
           this->ECount++;
       }

       this->Adj.append(tempList);
    }
    this->ECount = this->ECount / 2;

    fromADJtoAMR();//--перевод из списка смжности в матрицу смежности
    arrangementGraph(); //--менеджер координат
}
//--перевод из списка смежности в матрицу смежности
void Graph::fromADJtoAMR()
{
    int count = this->VCount;
    this->Amr = new int*[count];
    for(int i = 0; i < count; ++i)
    {
        Amr[i] = new int[count]{0};
        for (int k = 1; k < this->getAdjCount(i); ++k)  //--k == 1, т.к. adj[0] э V
             Amr[i][this->AdjAt(i,k)-1] = 1;
    }
}
//--обход в ширину
Graph Graph::bfs()
{
    Graph bfsGraph;                             //--граф поиска в ширину

    int source = 0;
    QList<int> queue;                           //--очередь вершин для обработки
    int cur;                                    //--указатель на текущую вершину
    queue.append(source);                       //--добавляем источник в очередь

    this->setVertexColor(source,0,Qt::green);

    //this->graphDesigner.repaint(graph.getVertex(source,0));
    int i = 0;
    int j = 0;

    while (!queue.isEmpty())                                //--пока очередь не пуста
    {
        QVector<Vertex> vec;

        cur = queue.back();                                 //--получим текущую вершину из очереди
        queue.removeLast();                                 //--удалим текущую вершину из очереди
        vec.append( this->getVertex(cur,0));
        for (j = 1; j < this->getAdjCount(cur); ++j)        //--пока в cur вершине есть  смежные узлы
        {

            i = this->getVertex(cur,j).number - 1;
            if(this->getVertex(i,0).color == Qt::white)     //--если узел не рассматривался(белый)
            {


                this->setVertexColor(i,0,Qt::green);     //--меняем цвет на серый

                vec.append( this->getVertex(i,0));        //--добавляем расматривамую вершину в выходной граф
                //this->graphDesigner.repaint(graph.getVertex(i,0));  //--выводим цвет
                queue.append(i);         //--добавляем указатель в очередь
            }
        }
        this->setVertexColor(cur,0,Qt::gray);
        //this->graphDesigner.repaint(graph.getVertex(cur,0));
        ++cur;


        bfsGraph.Adj.append(vec); //--добавляеем вектор в bfsGraph
    }

    return bfsGraph;
}
//--чистит цвет вершин
void Graph::clear()
{
    for (int i = 0; i < this->getVCount(); ++i)
    {
        for(int j = 0; j < this->getAdjCount(i); ++j)
        {
            setVertexColor(i,j, Qt::white);
        }
    }

}
//--Визуальная раскладка графа 
void Graph::arrangementGraph()
{
    //--менеджер координат

    //--рандомный подход
    //    for (int i = 0; i < graph->getVCount(); ++i)
    //    {
    //        int x = rand() % -550 + 50;
    //        int y = rand() % -350 + 500;
    //        graph->setVertexPos(i, 0, x, y);
    //    }

    int n = this->getVCount();
    for (int i = 0; i < n; ++i)
    {
        qreal fAngle = 2 * 3.14 * i / n;
        qreal x = 100 + cos(fAngle) * 150;
        qreal y = 100 + sin(fAngle) * 150;
        this->setVertexPos(i, 0, x, y);
    }
}
