#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFile>
#include <graph.h>
#include <future>
#include "graphdesigner.h"
#include "demonstrationstep.h"
#include <thread>
#include <iostream>
//  --  Демонстрация    --  //
using namespace std;        //стандартное пространство имён

//--DECLARE--
Graph graph;
Graph BFStree;
QList<DemonstrationStep> demList;
int index = 0;
QString HEAD; //--заголовки колонок для отображения процесса (меняются при выборе метода)


//--Меню: "Загрузить граф"
void MainWindow::on_loadGraph_triggered()
{
    graphInit();
}
//--Иниц.графа
void MainWindow::graphInit()
{
    try
    {
      graph.initFromFile("./resources/graph.txt");               //--иниц.графа
      graphDisplay();
    }
    catch(QString s)
    {
        QMessageBox::information(this, "graphInit", s);
    }

}

//--Вывод графа (табл,список смежности,изобр)
void MainWindow::graphDisplay()
{
    try
    {
        ui->adjBox->clear();        //--очистка панели
        ui->tableWidget->clear();   //--очистка таблицы

        graphInTable();
        graphInAdj();
        graphDraw();
    }
    catch(exception e)
    {
        QMessageBox::information(0,"graphDisplay", QString(e.what()));
    }

}
//--Вывод в виде списков смежности
void MainWindow::graphInAdj()
{
    //--расм. iый список
    for (int i = 0; i < graph.getVCount(); ++i)
    {
        ui->adjBox->append(QString("%1").arg(graph.AdjAt(i,0)));
        ui->adjBox->insertPlainText(" -");
        for (int j = 1; j < graph.getAdjCount(i); ++j)
        {
            ui->adjBox->insertPlainText("-> ");
            ui->adjBox->insertPlainText(QString("%1").arg(graph.AdjAt(i,j)));
        }
    }
    ui->adjBox->append("-----------------------");
    ui->adjBox->append(QString("Кол-во вершин: %1").arg(graph.getVCount()));
    ui->adjBox->append(QString("Кол-во рёбер: %1").arg(graph.getECount()));
}
//--Вывод в виде матрицы смежности
void MainWindow::graphInTable()
{
    int count = graph.getVCount();
    ui->tableWidget->setRowCount(count);
    ui->tableWidget->setColumnCount(count);

    for (int i = 0; i < count; ++i)
    {
        for (int j = 0; j < count; ++j)
        {
            QTableWidgetItem *item = new QTableWidgetItem();    //--выделени памяти под ячейку
            item->setText(QString("%1").arg(graph.AmrAt(i,j)));     //--иниц. ячейки
            ui->tableWidget->setItem(i, j, item);               //--вставка ячейки
            ui->tableWidget->setColumnWidth(j, 30);             //--ширина столбца
        }
    }
}
//--Графическое отображение графа
void MainWindow::graphDraw()
{
    try
    {
        this->graphDesigner.drawGraph(&graph);
    }
    catch(QException e)
    {
        QMessageBox::information(0,"Внимание", QString(e.what()));
    }

}

//--Обход графа в ширирну c цветом
void MainWindow::bfs()
{
    DemonstrationStep step;
    int source = 0;
    QList<int> queue;                                       //--очередь вершин для обработки
    QList<int> Gray;
    QList<int> Green;
    QList<int> Blue;


    addToQueue(&queue, &Green, source);                             //--добавляем источник в очередь
    Blue.append(source);
    demList.append(*step.init(-1, &Gray, &Green, &Blue, &queue, QString("Начинаем обход в ширирну с вершины %1").arg(source+1)));
   // demList.append(*step.init(-1, &Gray, &Green, &queue, QString("Помещаем вершину %1 в очередь").arg(source+1)));


    while (!queue.isEmpty())                                //--пока очередь не пуста
    {
        int cur = popQueue(&queue);                         //--получим текущую вершину из очереди
        Blue.append(cur);////синь
        demList.append(*step.init(cur, &Gray, &Green, &Blue, &queue,
                QString("Извлекаем вер. %1 из очереди. И рассм. её смежные вершины").arg(cur+1)));

        for (int j = 1; j < graph.getAdjCount(cur); ++j)    //--пока в текущей (cur) вершине есть  смежные узлы..
        {
            int tempV = graph.getVertex(cur,j).number - 1;  //--рассматриваем очередной смежный узел
            if(graph.getVertex(tempV,0).color == Qt::white) //--если узел не рассматривался(белый)
            {
                addToQueue(&queue,&Green, tempV);                  //--добавляем узел в очередь
                Green.append(tempV);////зелень
                demList.append(*step.init(-1, &Gray, &Green, &Blue, &queue,
                        QString("Помещаем вершину %1 в очередь - т.к. она не отмечена").arg(tempV+1)));
            }
        }

        graph.setVertexColor(cur,0,Qt::gray);               //--помечаем серым вершину, у которой ..
        Gray.append(cur);////cерь
        demList.append(*step.init(cur, &Gray, &Green, &Blue, &queue,
                QString("Помечаем вершину %1 как полностью рассмотренную").arg(cur+1)));

        ++cur;
    }
    demList.append(*step.init(-1, &Gray, &Green, &Blue, &queue,
            QString("Конец алгоритма")));
    graph.clear();
}

//--добавление в очередь
void MainWindow::addToQueue(QList<int> *queue,QList<int> *Green, int i)
{
    queue->append(i);
    graph.setVertexColor(i,0,Qt::green);
    Green->append(i);
}
//--удаляем и возвращаем из очереди 1 элемент
int MainWindow::popQueue(QList<int> *queue)
{
    int i = queue->first();                         //--получим текущую вершину из очереди
    queue->removeFirst();
    //queueDisplay(*queue, i, QString("Удаляем вершину %1 из очереди").arg(i+1));
    //graph.setVertexColor(i,0,Qt::green);
    //this->graphDesigner.repaint(graph.getVertex(i,0));
    //QMessageBox::information(this,"0",QString("%1: %2 "));

    return i;
}
//--вывод очереди на экран
void MainWindow::queueDisplay(QList<int> queue, int curr = -1, QString str = "fff")
{
    if (curr == -1)
        ui->tbQueue->setText(ui->tbQueue->toPlainText() + "\n \t");
    else
        ui->tbQueue->setText(ui->tbQueue->toPlainText() + "\n"  + QString::number(curr+1) + "\t");

    for (int i = 0; i < queue.count(); ++i)
    {

        ui->tbQueue->setText(ui->tbQueue->toPlainText() + "  " + QString::number(queue.at(i)+1));

    }
    ui->tbQueue->setText(ui->tbQueue->toPlainText() + "\t" + str);
    //QMessageBox::information(this,"0",QString("%1: %2 ").arg(queue.at(i)));
    //QMessageBox::information(this, "queueDisplay", "queueDisplay");

}
//--вывод очереди на экран
void MainWindow::stackDisplay(QList<int> stack, QString str = "fff")
{
    for (int i = 0; i < stack.count(); ++i)
    {

        ui->tbQueue->setText(ui->tbQueue->toPlainText() + "  " + QString::number(stack.at(i)+1));

    }
    ui->tbQueue->setText(ui->tbQueue->toPlainText() + "\t\t" + str +"\n");
}

//--Отображение шага
void MainWindow::stepDisplay(int i)
{
   if(demList.count() > i)
   {
        ui->tbQueue->clear();
        ui->tbQueue->setText(HEAD);
        for (int j = 0; j <= i; ++j)
        {
            //->tbQueue->setText(ui->tbQueue->toPlainText() + QString("%1 \t").arg(j));
            if (ui->bfsRadio->isChecked())
                queueDisplay(demList.at(j).queue, demList.at(j).curNode, demList.at(j).comment);
            else if(ui->dfsRadio->isChecked())
                stackDisplay(demList.at(j).queue, demList.at(j).comment);
        }

        //--Перекраска
        graph.clear();      //--чистим цвета
        for (int f = 0; f <= i; ++f)
        {
            //--красим зеленое
            for (int q = 0; q < demList.at(f).GreenNode.count(); ++q)
            {
                graph.setVertexColor(demList.at(f).GreenNode.at(q), 0, Qt::green);
            }
            for (int s = 0; s < demList.at(f).BlueNode.count(); ++s)
            {
                graph.setVertexColor(demList.at(f).BlueNode.at(s), 0, Qt::blue);
            }
            //--красим серое
            for (int g = 0; g < demList.at(f).GreyNode.count(); ++g)
            {
                graph.setVertexColor(demList.at(f).GreyNode.at(g), 0, Qt::gray);
            }
        }
        graphDesigner.drawGraph(&graph);

   }
}
//--Вывод количества шагов на экран "i шаг из N"
void MainWindow::stepCounterDisplay(int i, int n)
{
    ui->labelIndex->setText(QString("%1 шаг из %2").arg(i+1).arg(n+1));
}
//--кнопка "шаг вперед"
void MainWindow::on_pbStepForward_clicked()
{
    try
    {
        //--если не выбран метод
        if (!ui->bfsRadio->isChecked() && !ui->dfsRadio->isChecked())
            return;
        //--если выход за пределы листа шагов
        if (index < demList.count())
            ++index;
        //--если выбран обход в ширину
        if (ui->bfsRadio->isChecked())
        {
            stepDisplay(index);
        }
        //--если выбран обход в глубину
        else if (ui->dfsRadio->isChecked())
        {
            stepDisplay(index);
            //
        }
        stepCounterDisplay(index, demList.count());
    }
    catch(const QString s)
    {
        QMessageBox::information(0,"Exception",s);
    }
}
//--кнопка "шаг назад"
void MainWindow::on_pbStepBack_clicked()
{
    try
    {
        if (!ui->bfsRadio->isChecked() && !ui->dfsRadio->isChecked())
            return;

        if (index >= 0)
            --index;

        if (ui->bfsRadio->isChecked())
        {
            stepDisplay(index);
        }
        else if (ui->dfsRadio->isChecked())
        {

            stepDisplay(index);
        }
        stepCounterDisplay(index, demList.count());
    }
    catch(const QString s)
    {
        QMessageBox::information(0,"Exception",s);
    }
}

//--обход в ширину, радиокнопка
void MainWindow::on_bfsRadio_clicked()
{
    HEAD = "Узел \t Очередь \t Комментарий \n";
    ui->tbQueue->setText(HEAD);
    index = -1;
    demList.clear();
    graph.clear();
    bfs();
    stepCounterDisplay(index, demList.count());
    graphDesigner.drawGraph(&graph);

}
//--обход в глубину, радиокнопка
void MainWindow::on_dfsRadio_clicked()
{
    HEAD = "Cтек \t\t Комментарий \n";
    ui->tbQueue->setText(HEAD);
    index = -1;
    graph.clear();
    demList.clear();
    dfs();
    stepCounterDisplay(index, demList.count());
    graphDesigner.drawGraph(&graph);
}


//--Обход графа в глубину c цветом

void MainWindow::dfs()
{
    DemonstrationStep step;
    QList<int> stack;                                       //--cтек вершин для обработки
    QList<int> Gray2;
    QList<int> Green2;
    QList<int> Blue2;
    int curV = 0;                               //--обход графа начиная с вершины curV
    stack.append(curV);                         //--добавляем вершину в стек
    graph.setVertexColor(curV,0,Qt::green);     //--помечаем вершину как посещенную

    Green2.append(curV);
    demList.append(*step.init(-1, &Gray2, &Green2, &Blue2, &stack, QString("Начинаем обход в ширирну с вершины %1").arg(curV+1)));

    int tempV;
    while (!stack.empty())                              //--пока стек не пуст
    {
        curV = stack.back();

        if (dfcCheck(curV))
        {
            graph.setVertexColor(curV,0,Qt::gray);
            Gray2.append(curV);
            stack.pop_back();
            demList.append(*step.init(-1, &Gray2, &Green2, &Blue2, &stack, QString("Вершина %1 не имеет смежных не посещенных точек (откат)").arg(curV+1)));

        }
        else
        {
            for(int i = 1; i < graph.getAdjCount(curV); ++i)
            {
                tempV = graph.getVertex(curV,i).number - 1;
                if (graph.getVertex(tempV,0).color == Qt::white)
                {

                    graph.setVertexColor(tempV,0,Qt::green);
                    Green2.append(tempV);
                    stack.append(tempV);
                    demList.append(*step.init(-1, &Gray2, &Green2, &Blue2, &stack, QString("Помещаем %1 в стек").arg(tempV+1)));
                }
            }
        }
    }
    demList.append(*step.init(-1, &Gray2, &Green2, &Blue2, &stack,
            QString("Конец алгоритма")));
    Gray2.clear();
    Green2.clear();
    stack.clear();
    graph.clear();
}
bool MainWindow::dfcCheck(int v)
{
    for(int i = 1; i < graph.getAdjCount(v); ++i)
    {
        int tempV = graph.getVertex(v,i).number - 1;
        if (graph.getVertex(tempV,0).color == Qt::white)
            return false;
    }
    return true;
}
