#ifndef QUEUE_H
#define QUEUE_H

struct Node //описание узла списка
{
    int data; //информационное поле
    Node *next; //указатель на следующий элемент
};

class Queue//описание очереди
{
    int size; //счетчик размера очереди
    Node *first; //указатель на начало очереди
    Node *last; //указатель на конец очереди
public:
    Queue();
};

#endif // QUEUE_H
